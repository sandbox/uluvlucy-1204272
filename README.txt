Google Analytics Chart (GA Chart) displays interactive graphs of your Google
Analytics data, similar to the Google Analytics Dashboard.

In order to use it, you must have a Google Analytics account (free to create at
http://www.google.com/analytics/ ) and an active report installed on a site.
You are not required to use the Google Analytics module.

The main chart is a timeline of all visits to your site between now and when
you started recording traffic data (or some other start date of your choice).

The secondary charts show visitors data and browser usage by visits. This
section is independently controlled by a date range selection form.

The module also displays 2 tables. The first summarizes your site's usage stats
(visits, pageviews, pages/visit, bounce rate, average time on site, and 
percent of new visits), while the second lists the top 25 pages visited on
your site.

Upon installation, you must visit your GA Chart settings page
(admin/settings/gachart) and fill in your Google Analytics account information
before you may begin using the module's features. You will need to enter your
Google Account name (in the form of address@gmail.com), your password (it will
be stored in your site's database and will not be used anywhere else), and the
ID of the Google Analytics report you wish to view (which can be found by
viewing the report from within http://google.com/analytics/, looking at the
address bar, and copying the number that immediately follows the &id= section
of the URL).

After the initial setup, a "start date" field will appear in the settings form.
GA Chart automatically detects the earliest date for which your report has data,
but you may choose a different starting date if you desire.

If you have installed the module and cannot find the report in the menu
(admin/reports/gachart), remember to clear your cache.

Additionally, the live fetching of data from a third party site (Google) causes
a noticeable delay in the load time required for the report (~30s). If you have
suggestions for ways to enhance the module's speed, please leave a comment on
its Drupal project page.

This module was built using the Google Analytics PHP Interface (GAPI)
(http://code.google.com/p/gapi-google-analytics-php-interface/) and the Google
Chart Tools API (http://code.google.com/apis/chart/) available on Google Code.
Because this module's operation is dependent upon a third party , it is only as
reliable as your access to Google.
